import org.apache.commons.cli.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class MyCP {
    public static void main(String[] args) {
        // Create a Parser
        CommandLineParser parser = new DefaultParser();
        Option optiontx=new Option("tx","txt2bin",true,"Convert .txt to .bin");
        optiontx.setArgs(2);
        optiontx.setRequired(false);
        Option optionxt=new Option("xt","bin2txt",true,"Convert .bin to .txt");
        optionxt.setArgs(2);
        optionxt.setRequired(false);
        Options options = new Options();
        options.addOption(optiontx);
        options.addOption(optionxt);
        File srcfile,dstfile;
        try{
            // Parse the program arguments
            CommandLine commandLine = parser.parse(options, args);
            // Set the appropriate variables based on supplied options
            if(commandLine.hasOption("tx")) {
                srcfile = new File(commandLine.getOptionValues("tx")[0]);
                dstfile = new File(commandLine.getOptionValues("tx")[1]);
                FileOutputStream output = new FileOutputStream(dstfile);
                Scanner sc = new Scanner(srcfile);
                while (sc.hasNext()){
                    if (sc.hasNextInt()){
                        output.write(Integer.toBinaryString(sc.nextInt()).getBytes());
                        output.write((byte)0x20);
                    } else {
                        sc.next();
                    }
                }
            } else if (commandLine.hasOption("xt")) {
                srcfile = new File(commandLine.getOptionValues("xt")[0]);
                dstfile = new File(commandLine.getOptionValues("xt")[1]);
                FileOutputStream output = new FileOutputStream(dstfile);
                Scanner sc = new Scanner(srcfile);
                while (sc.hasNext()){
                    if (sc.hasNextInt()){
                        output.write(Integer.toString(sc.nextInt(2)).getBytes());
                        output.write((byte)0x20);
                    } else {
                        sc.next();
                    }
                }
            } else {
                if (commandLine.getArgs().length!=2){
                    System.out.println("Missing argument for MyCP");
                    System.exit(1);
                }
                srcfile = new File(commandLine.getArgs()[0]);
                dstfile = new File(commandLine.getArgs()[1]);
                FileInputStream input = new FileInputStream(srcfile);
                FileOutputStream output;
                if (dstfile.isDirectory()){
                    String filename = srcfile.toString();
                    if(filename.contains("/")){
                        filename = srcfile.toString().substring(srcfile.toString().lastIndexOf("/"));
                    }
                    output = new FileOutputStream(dstfile+"/"+filename);
                } else {
                    if(srcfile.equals(dstfile)){
                        System.out.println(dstfile.toString()+" is the same file with "+dstfile.toString());
                        System.exit(2);
                    }
                    dstfile.createNewFile();
                    output = new FileOutputStream(dstfile);
                }
                byte[] buf = new byte[1024];
                int length;
                while ((length=input.read(buf))>0){
                    output.write(buf,0,length);
                }
                input.close();
                output.close();
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
