import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.Scanner;

import static org.junit.Assert.*;

public class MyCPTest {
    private boolean testSameFile(String[] args) throws Exception {
        FileInputStream File1 = new FileInputStream(args[0]);
        FileInputStream File2 = new FileInputStream(args[1]);
        boolean flag;
        if (File1.available() != File2.available()) {
            //长度不同内容肯定不同
            flag = false;
        } else {
            while (File1.read() != -1 && File2.read() != -1) {
                if (File1.read() != File2.read()) {
                    flag = false;
                }
            }
            flag = true;
        }
        File1.close();
        File2.close();
        return flag;
    }

    @Before
    public void init(){
        File file = new File("1.txt");
        File numFile = new File("number.txt");
        File binaryFile = new File("binary.bin");
        try{
            file.createNewFile();
            numFile.createNewFile();
            binaryFile.createNewFile();
            FileOutputStream output = new FileOutputStream(file);
            output.write("MyCP works perfectly!!".getBytes());
            output = new FileOutputStream(numFile);
            output.write("10 a 9".getBytes());
            output = new FileOutputStream(binaryFile);
            output.write("1111 10 1001".getBytes());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void testNoOpt() {
        String[] args1 = {"1.txt", "2.txt"};
        String[] args2 = {"1.txt", "src"};
        MyCP.main(args1);
        MyCP.main(args2);
        try{
            assertTrue(testSameFile(args1));
            args2[1]="src/1.txt";
            assertTrue(testSameFile(args2));
        }catch (Exception e){
            System.out.println(e.getMessage());
            fail();
        }
    }

    @Test
    public void testTX(){
        String[] args = {"-tx","number.txt","2.bin"};
        MyCP.main(args);
        try{
            FileInputStream bin = new FileInputStream("2.bin");
            Scanner sc = new Scanner(bin);
            assertEquals(sc.nextLine(),"1010 1001 ");
        }catch (Exception e){
            fail();
        }
    }

    @Test
    public void testXT(){
        String[] args = {"-xt","binary.bin","decimal.txt"};
        MyCP.main(args);
        try{
            FileInputStream bin = new FileInputStream("decimal.txt");
            Scanner sc = new Scanner(bin);
            assertEquals(sc.nextLine(),"15 2 9 ");
        }catch (Exception e){
            fail();
        }
    }
}